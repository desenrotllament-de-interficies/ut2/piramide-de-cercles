module org.example {
    requires javafx.controls;
    requires javafx.fxml;

    opens org.example to javafx.fxml;
    opens Contenidors to javafx.fxml;

    exports Contenidors;
    exports org.example;
}