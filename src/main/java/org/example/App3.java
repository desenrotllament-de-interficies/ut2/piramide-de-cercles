package org.example;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Group;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.paint.Color;
import javafx.scene.shape.Rectangle;
import javafx.stage.Stage;
import javafx.scene.shape.Circle;

import java.io.IOException;


/**
 * JavaFX App
 */
public class App3 extends Application {

    private static Scene scene;

    @Override
    public void start(Stage theStage) throws IOException {
        theStage.setTitle("Tablero3");
        Group root = new Group();

        //Cordenadas
        int y = 20;
        int x = 200;


        for (int i = 0; i < 8; i++) {
            if (i != 0) {
                y = y + 40;
                x = x - 20;
            }
            for (int j = 0; j < i; j++) {

                Circle circulo = new Circle(20);
                circulo.setCenterX(x+(j*40));
                circulo.setCenterY(y);
                circulo.setFill(Color.valueOf("#bc0090"));
                circulo.setStrokeWidth(3);
                root.getChildren().add(circulo);

            }


        }

        Scene theScene = new Scene(root, 400, 400);
        theStage.setScene(theScene);
        theStage.show();

    }

    static void setRoot(String fxml) throws IOException {
        scene.setRoot(loadFXML(fxml));
    }

    private static Parent loadFXML(String fxml) throws IOException {
        FXMLLoader fxmlLoader = new FXMLLoader(App3.class.getResource(fxml + ".fxml"));
        return fxmlLoader.load();
    }

    public static void main(String[] args) {
        launch();
    }

}