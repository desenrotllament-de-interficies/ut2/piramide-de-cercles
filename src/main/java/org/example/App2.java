package org.example;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Group;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.paint.Color;
import javafx.scene.shape.Rectangle;
import javafx.stage.Stage;

import java.io.IOException;


/**
 * JavaFX App
 */
public class App2 extends Application {

    private static Scene scene;

    @Override
    public void start(Stage theStage) throws IOException {
        theStage.setTitle("Tablero2");
        Group root = new Group();

        for (int i = 0; i < 8; i++) {
            for (int j = 0; j < 8; j++) {
                Rectangle cuadrado = new Rectangle(50, 50);
                cuadrado.setId((i + 1) + "-" + (j + 1));

                cuadrado.setX(i * 50);
                cuadrado.setY(j * 50);
                if (i % 2 == 1 && j % 2 == 0 || i % 2 == 0 && j % 2 == 1) {
                    cuadrado.setFill(Color.WHITE);
                } else {
                    cuadrado.setFill(Color.BLUE);
                }
                cuadrado.setOnMouseClicked(event ->
                {
                    Alert alert = new Alert(Alert.AlertType.INFORMATION);
                    alert.setTitle("Informacion");
                    alert.setHeaderText("Coordenada");
                    alert.setContentText("Has hecho click en la casilla " + cuadrado.getId());
                    alert.showAndWait();


                });


                root.getChildren().add(cuadrado);
            }


        }

        Scene theScene = new Scene(root, 400, 400);
        theStage.setScene(theScene);
        theStage.show();

    }

    static void setRoot(String fxml) throws IOException {
        scene.setRoot(loadFXML(fxml));
    }

    private static Parent loadFXML(String fxml) throws IOException {
        FXMLLoader fxmlLoader = new FXMLLoader(App2.class.getResource(fxml + ".fxml"));
        return fxmlLoader.load();
    }

    public static void main(String[] args) {
        launch();
    }

}