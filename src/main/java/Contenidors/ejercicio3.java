package Contenidors;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;

import javafx.scene.Group;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;

import java.io.IOException;

public class ejercicio3 extends Application {
    private static Scene scene;


    public void start(Stage theStage) {
        theStage.setTitle("AnchorPane");
        Group root = new Group();
        AnchorPane anchorPa = new AnchorPane();

        Button boton1 = new Button("Elemento situado en top+left");
        Button boton2 = new Button("Elemento situado en top+left");
        Button boton3 = new Button("Elemento situado en top+right");
        Button boton4 = new Button("Elemento situado en top+left+right+bottom");

        AnchorPane.setTopAnchor(boton1,40.0);
        AnchorPane.setLeftAnchor(boton1,50.0);

        AnchorPane.setTopAnchor(boton2,90.0);
        AnchorPane.setLeftAnchor(boton2,10.0);
        AnchorPane.setRightAnchor(boton2,320.0);

        AnchorPane.setTopAnchor(boton3,100.0);
        AnchorPane.setRightAnchor(boton3,20.0);

        AnchorPane.setTopAnchor(boton4,150.0);
        AnchorPane.setLeftAnchor(boton4,40.0);
        AnchorPane.setRightAnchor(boton4,50.0);
        AnchorPane.setBottomAnchor(boton4,45.0);

        anchorPa.getChildren().addAll(boton1, boton2,boton3,boton4);





        //INSERTAMOS EL GRIDPANE AL GRUPO PARA METERLO A LA ESCENA Y MOSTRARLA
        root.getChildren().add(anchorPa);
        Scene theScene = new Scene(root, 600, 400);
        theStage.setScene(theScene);
        theStage.show();


    }
    static void setRoot(String fxml) throws IOException {
        scene.setRoot(loadFXML(fxml));
    }

    private static Parent loadFXML(String fxml) throws IOException {
        FXMLLoader fxmlLoader = new FXMLLoader(ejercicio1.class.getResource(fxml + ".fxml"));
        return fxmlLoader.load();
    }

    public static void main(String[] args) {
        launch();

    }


}