package Contenidors;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.geometry.Insets;
import javafx.scene.Group;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;

import java.io.IOException;

public class ejercicio4 extends Application {
    private static Scene scene;


    public void start(Stage theStage) {
        theStage.setTitle("VBox Hbox");
        Group root = new Group();
        VBox vbox = new VBox();
        HBox hbox = new HBox();

        Label etiqueta = new Label("Etiqueta VBOX");
        Button boton1 = new Button("Button1 VBOX");
        Button boton2 = new Button("Button2 VBOX");
        TextField tf = new TextField("Text field VBOX");
        RadioButton botonR = new RadioButton("Radio button1 VBOX");
        CheckBox chbox = new CheckBox(" check box VBOX");

        VBox.setMargin(etiqueta, new Insets(10));
        VBox.setMargin(boton1, new Insets(10));
        VBox.setMargin(boton2, new Insets(10));
        VBox.setMargin(tf, new Insets(10));
        VBox.setMargin(botonR, new Insets(10));
        VBox.setMargin(chbox, new Insets(10));

/*
        HBox.setMargin(etiqueta, new Insets(10));
        HBox.setMargin(boton1, new Insets(10));
        HBox.setMargin(boton2, new Insets(10));
        HBox.setMargin(tf, new Insets(10));
        HBox.setMargin(botonR, new Insets(10));
        HBox.setMargin(chbox, new Insets(10));


hbox.setPadding(new Insets(20));
hbox.getChildren().addAll(etiqueta, boton1, boton2, tf, botonR, chbox);

*/
        boton1.setPadding(new Insets(150));
        vbox.setPadding(new Insets(20));


        vbox.getChildren().addAll(etiqueta, boton1, boton2, tf, botonR, chbox);



        root.getChildren().add(vbox);
        Scene theScene = new Scene(root, 600, 700);
        theStage.setScene(theScene);
        theStage.show();


    }

    static void setRoot(String fxml) throws IOException {
        scene.setRoot(loadFXML(fxml));
    }

    private static Parent loadFXML(String fxml) throws IOException {
        FXMLLoader fxmlLoader = new FXMLLoader(ejercicio1.class.getResource(fxml + ".fxml"));
        return fxmlLoader.load();
    }

    public static void main(String[] args) {
        launch();

    }

}
