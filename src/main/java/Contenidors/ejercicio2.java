package Contenidors;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.geometry.HPos;
import javafx.geometry.Insets;
import javafx.scene.Group;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.layout.BorderPane;
import javafx.stage.Stage;

import java.io.IOException;

public class ejercicio2 extends Application {
    private static Scene scene;


    public void start(Stage theStage) {
        theStage.setTitle("PaneBorder,");
        Group root = new Group();
        BorderPane paneBorder = new BorderPane();
        paneBorder.setPadding(new Insets(15, 20, 10, 10));

        Button boton1 = new Button("Elemento situado en top");

        boton1.setPadding(new Insets(10, 10, 10, 10));
        Button boton2 = new Button("Elemento situado en left");
        boton2.setPadding(new Insets(10, 10, 10, 10));
        Button boton3 = new Button("Elemento situado en Center");
        boton3.setPadding(new Insets(10, 10, 10, 10));
        Button boton4 = new Button("Elemento situado en Right");
        boton4.setPadding(new Insets(10, 10, 10, 10));
        Button boton5 = new Button("Elemento situado en Bottom");
        boton5.setPadding(new Insets(10, 10, 10, 10));



        paneBorder.setTop(boton1);
        paneBorder.setLeft(boton2);
        paneBorder.setCenter(boton3);
        paneBorder.setRight(boton4);
        paneBorder.setBottom(boton5);

        BorderPane.setMargin(boton1, new Insets(10));
        BorderPane.setMargin(boton2, new Insets(10));
        BorderPane.setMargin(boton3,new Insets(10));
        BorderPane.setMargin(boton4,new Insets(10));
        BorderPane.setMargin(boton5,new Insets(10));

        //INSERTAMOS EL GRIDPANE AL GRUPO PARA METERLO A LA ESCENA Y MOSTRARLA
        root.getChildren().add(paneBorder);
        Scene theScene = new Scene(root, 600, 600);
        theStage.setScene(theScene);
        theStage.show();


    }
    static void setRoot(String fxml) throws IOException {
        scene.setRoot(loadFXML(fxml));
    }

    private static Parent loadFXML(String fxml) throws IOException {
        FXMLLoader fxmlLoader = new FXMLLoader(ejercicio1.class.getResource(fxml + ".fxml"));
        return fxmlLoader.load();
    }

    public static void main(String[] args) {
        launch();

    }


}
