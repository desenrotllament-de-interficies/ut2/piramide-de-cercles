package Contenidors;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.geometry.Insets;
import javafx.scene.Group;
import javafx.scene.Parent;
import javafx.scene.Scene;

import javafx.scene.control.*;
import javafx.scene.layout.FlowPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;


import java.io.IOException;

public class ejercicio5 extends Application {
    private static Scene scene;


    public void start(Stage theStage) {
        theStage.setTitle("flowPane,");
        Group root = new Group();



        FlowPane flow = new FlowPane();


        for (int i = 0; i < 8; i++) {
            flow.getChildren().add(getVbox());
        }

        root.getChildren().add(flow);
        Scene theScene = new Scene(root, 900, 900);
        theStage.setScene(theScene);
        theStage.show();


    }
    static void setRoot(String fxml) throws IOException {
        scene.setRoot(loadFXML(fxml));
    }

    private static Parent loadFXML(String fxml) throws IOException {
        FXMLLoader fxmlLoader = new FXMLLoader(ejercicio1.class.getResource(fxml + ".fxml"));
        return fxmlLoader.load();
    }

    public static void main(String[] args) {
        launch();

    }

    public VBox getVbox(){
        VBox vbox = new VBox();


        Label etiqueta = new Label("Etiqueta VBOX");
        Button boton1 = new Button("Button1 VBOX");
        Button boton2 = new Button("Button2 VBOX");
        TextField tf = new TextField("Text field VBOX");
        RadioButton botonR = new RadioButton("Radio button1 VBOX");
        CheckBox chbox = new CheckBox(" check box VBOX");

        VBox.setMargin(etiqueta, new Insets(10));
        VBox.setMargin(boton1, new Insets(10));
        VBox.setMargin(boton2, new Insets(10));
        VBox.setMargin(tf, new Insets(10));
        VBox.setMargin(botonR, new Insets(10));
        VBox.setMargin(chbox, new Insets(10));
        vbox.setStyle( "-fx-border-color:black; \n " );
        vbox.getChildren().addAll(etiqueta, boton1, boton2, tf, botonR, chbox);

        return vbox;
    }

}
