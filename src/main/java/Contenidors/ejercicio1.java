package Contenidors;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.geometry.HPos;
import javafx.geometry.Insets;
import javafx.scene.Group;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.layout.GridPane;
import javafx.stage.Stage;

import java.io.IOException;

public class ejercicio1 extends Application {
    private static Scene scene;


    public void start(Stage theStage) {
        theStage.setTitle("GridPane");
        Group root = new Group();
        GridPane gridpane = new GridPane();
        gridpane.setPadding(new Insets(20, 0, 20, 20));
        gridpane.setHgap(7);
        gridpane.setVgap(7);

        //CREACION DE LOS LABELS
        Label nom = new Label("Nombre:");
        Label cognom = new Label("Cognom:");
        Label ciutat = new Label("Ciutat:");
        Label direccio = new Label("Direccio:");
        Label descripcio = new Label("Descripcio:");

        //COLOCAMOS POSICION DERECHA A LOS LABELS
        gridpane.setHalignment(nom, HPos.RIGHT);
        gridpane.setHalignment(cognom, HPos.RIGHT);
        gridpane.setHalignment(ciutat, HPos.RIGHT);
        gridpane.setHalignment(direccio, HPos.RIGHT);
        gridpane.setHalignment(descripcio, HPos.RIGHT);

        //CREACUIB DE LOS IMPUTS
        TextField tfnom = new TextField();
        TextField tfcognom = new TextField();
        TextField tfciutat = new TextField();
        TextField tfdireccio = new TextField();
        TextArea tAdescripcio = new TextArea();

        //PONEMOS LA COLOCACION DE CADA LABEL
        gridpane.add(nom, 1, 1);
        gridpane.add(cognom, 1, 2);
        gridpane.add(ciutat, 1, 3);
        gridpane.add(direccio, 1, 4);
        gridpane.add(descripcio, 1, 5);

        //COLOCACION DE CADA INPUT O CAMPO
        gridpane.add(tfnom, 2, 1);
        gridpane.add(tfcognom, 2, 2);
        gridpane.add(tfciutat, 2, 3);
        gridpane.add(tfdireccio, 2, 4);
        gridpane.add(tAdescripcio, 2, 5);

        //SEPARACION Y LOS LABELS
        gridpane.setColumnSpan(nom, 1);
        gridpane.setRowSpan(nom, 1);

        //INSERTAMOS EL GRIDPANE AL GRUPO PARA METERLO A LA ESCENA Y MOSTRARLA
        root.getChildren().add(gridpane);
        Scene theScene = new Scene(root, 700, 400);
        theStage.setScene(theScene);
        theStage.show();


    }

    static void setRoot(String fxml) throws IOException {
        scene.setRoot(loadFXML(fxml));
    }

    private static Parent loadFXML(String fxml) throws IOException {
        FXMLLoader fxmlLoader = new FXMLLoader(ejercicio1.class.getResource(fxml + ".fxml"));
        return fxmlLoader.load();
    }

    public static void main(String[] args) {
        launch();

    }

}

